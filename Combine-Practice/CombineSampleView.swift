//
//  CombineSampleView.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/24.
//

import SwiftUI

struct CombineSampleView: View {
  @StateObject var viewModel: CombineSampleViewModel
  
  init(viewModel: CombineSampleViewModel = CombineSampleViewModel()) {
    _viewModel = StateObject(wrappedValue: viewModel)
  }
    var body: some View {
      VStack {
        Button(action: {
          viewModel.aaa()
        }) {
          Text(viewModel.title)
            .padding()
        }
        
        Button(action: {
          viewModel.bbb()
        }) {
          Text(viewModel.title2)
            .padding()
        }
      }
      .onAppear {
        
      }
    }
}

struct CombineSampleView_Previews: PreviewProvider {
    static var previews: some View {
        CombineSampleView()
    }
}
