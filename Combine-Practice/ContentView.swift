//
//  ContentView.swift
//  Combine-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import SwiftUI
import ComposableArchitecture

struct ContentView: View {
  var body: some View {
    NavigationView {
      List {
        NavigationLink("Combine Sample") {
          CombineSampleView()
        }
        
        NavigationLink("API with Combine") {
          AFCombineView()
        }
        
        NavigationLink("API with Combine and Alamofire") {
          AFCombineView()
        }
        
        NavigationLink("API with TCA") {
          TCASampleView(
            store:
              Store(
                initialState: AppState(),
                reducer: appReducer,
                environment: AppEnvironment(
                  userClient: .live,
                  mainQueue: .main
                )
              )
          )
        }
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView(

    )
  }
}
