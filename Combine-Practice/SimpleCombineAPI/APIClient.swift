//
//  APIClient.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/22.
//

import Foundation
import Combine
import Alamofire

class APIClient {
  let decoder: JSONDecoder = {
    let jsonDecoder = JSONDecoder()
    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
    return jsonDecoder
  }()
  
  var anyCancellables: Set<AnyCancellable> = []
  deinit {
    anyCancellables.removeAll()
  }
  
  func fetchUser(url: URL) -> Future<TestUser, Error> {
    return Future { promise in
      let request = URLRequest(url: url)
      URLSession.shared
        .dataTaskPublisher(for: request)
        .map( { (data, response) in
          return data
        })
        .decode(type: TestUser.self, decoder: self.decoder)
        .sink(receiveCompletion: { completion in
          switch completion {
          case .failure(let error):
            print(error.localizedDescription)
            promise(.failure(NetworkError.userError))
          case .finished:
            print("completion")
          }
        }, receiveValue: { user in
          promise(.success(user))
        })
        .store(in: &self.anyCancellables)
    }
  }
}

enum NetworkError: Error {
  case userError
}
