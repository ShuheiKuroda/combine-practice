//
//  AFCombineModel.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/22.
//

import Foundation
import Combine

struct TestUser: Codable, Identifiable {
  var id: Int
  var name: String
  var age: Int
}

class AFCombineModel {

  func fetchUser() -> AnyPublisher<TestUser, Error>  {
    let urlStr = "https://run.mocky.io/v3/de739466-7439-41cb-b9b9-514448ab26ae"
    
    return APIAccessPublisher.publish(urlStr).eraseToAnyPublisher()
  }
}
