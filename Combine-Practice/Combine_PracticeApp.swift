//
//  Combine_PracticeApp.swift
//  Combine-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import SwiftUI

@main
struct Combine_PracticeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
