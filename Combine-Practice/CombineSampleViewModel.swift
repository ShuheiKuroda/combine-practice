//
//  CombineSampleViewModel.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/24.
//

import Foundation
import Combine

class CombineSampleViewModel: ObservableObject {
  @Published var title = "Int to String"
  @Published var title2 = "Combine with Alamofire"
  var count = 0
  let test = Test()
  var anyCancellables = Set<AnyCancellable>()
  
  enum ExampleError: Error {
    case example
  }
  
  init() {
    // mapの確認
    test.subject
      .map { (arg: Int) -> String in
        "value: \(arg)"
      }
      .sink(receiveValue: { value in
        print(value)
        self.title = value
    })
      .store(in: &anyCancellables)
  }
  
  func aaa() {
//    // mapの確認
//    test.subject
//      .map { (arg: Int) -> String in
//        "value: \(arg)"
//      }
//      .sink(receiveValue: { value in
//        print(value)
//        self.title = value
//    })
//      .store(in: &anyCancellables)
    test.subject.send(2)
  }
  
  func bbb() {
    // エラーが流れて異常終了しても、上流は異常終了しないパターン
    (1..<100).publisher
      .handleEvents(receiveOutput: {
        print("handleEventsA", $0)
      })
      .flatMap { arg -> AnyPublisher<Int, ExampleError> in
        if arg == 2 {
          return Fail(outputType: Int.self, failure: ExampleError.example)
            .eraseToAnyPublisher()
        } else {
          return Just(arg)
            .setFailureType(to: ExampleError.self)
            .eraseToAnyPublisher()
        }
      }
      .handleEvents(receiveOutput: {
        print("handleEventsB", $0)
      })
      .sink(receiveCompletion: {
        switch $0 {
        case .finished:
          print("正常終了")
        case .failure(let error):
          print("異常終了")
        }
      }, receiveValue: {
        print($0)
      })
      .store(in: &anyCancellables)
  }
}

class Test {
  let subject = PassthroughSubject<Int, Never>()
}
